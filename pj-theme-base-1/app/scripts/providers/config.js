(function (angular) {
    'use strict';
    angular.module('yapp').provider('GlobalConfig', function () {

        var values = {
            urlLoadDevices:'http://localhost:53568',
            loadDevicesDemo :true,            
            deviceDemo:[
            ]
            
        }
        return {
            $get: function () {
                return values;
            },
            set: function (constants) {
                angular.extend(values, constants);
            }
        };
    });
})(angular);