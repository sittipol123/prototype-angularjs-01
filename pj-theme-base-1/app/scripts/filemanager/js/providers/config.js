(function(angular) {
    'use strict';
    angular.module('FileManagerApp').provider('fileManagerConfig', function() {

        var values = {
            appName: 'angular-filemanager v1.5',
            defaultLang: 'en',

            listUrl: 'http://localhost/ftpapi/handler.php',
            uploadUrl: 'http://localhost/ftpapi/handler.php',
            renameUrl: 'http://localhost/ftpapi/handler.php',
            copyUrl: 'http://localhost/ftpapi/handler.php',
            moveUrl: 'http://localhost/ftpapi/handler.php',
            removeUrl: 'http://localhost/ftpapi/handler.php',
            editUrl: 'http://localhost/ftpapi/handler.php',
            getContentUrl: 'http://localhost/ftpapi/handler.php',
            createFolderUrl: 'http://localhost/ftpapi/handler.php',
            downloadFileUrl: 'http://localhost/ftpapi/handler.php',
            downloadMultipleUrl: 'http://localhost/ftpapi/handler.php',
            compressUrl: 'http://localhost/ftpapi/handler.php',
            extractUrl: 'http://localhost/ftpapi/handler.php',
            permissionsUrl: 'http://localhost/ftpapi/handler.php',

            searchForm: true,
            sidebar: true,
            breadcrumb: true,
            allowedActions: {
                upload: true,
                rename: false,
                move: true,
                copy: false,
                edit: false,
                changePermissions: true,
                compress: false,
                compressChooseName: false,
                extract: false,
                download: true,
                downloadMultiple: true,
                preview: true,
                remove: true
            },

            multipleDownloadFileName: 'angular-filemanager.zip',
            showSizeForDirectories: false,
            useBinarySizePrefixes: false,
            downloadFilesByAjax: true,
            previewImagesInModal: true,
            enablePermissionsRecursive: true,
            compressAsync: false,
            extractAsync: false,

            isEditableFilePattern: /\.(txt|diff?|patch|svg|asc|cnf|cfg|conf|html?|.html|cfm|cgi|aspx?|ini|pl|py|md|css|cs|js|jsp|log|htaccess|htpasswd|gitignore|gitattributes|env|json|atom|eml|rss|markdown|sql|xml|xslt?|sh|rb|as|bat|cmd|cob|for|ftn|frm|frx|inc|lisp|scm|coffee|php[3-6]?|java|c|cbl|go|h|scala|vb|tmpl|lock|go|yml|yaml|tsv|lst)$/i,
            isImageFilePattern: /\.(jpe?g|gif|bmp|png|svg|tiff?)$/i,
            isExtractableFilePattern: /\.(gz|tar|rar|g?zip)$/i,
            tplPath:  'scripts/filemanager/templates'
        };

        return {
            $get: function() {
                return values;
            },
            set: function (constants) {
                angular.extend(values, constants);
            }
        };

    });
})(angular);
