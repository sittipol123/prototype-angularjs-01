'use strict';

/**
 * @ngdoc overview
 * @name yapp
 * @description
 * # yapp
 *
 * Main module of the application.

 */
angular
  .module('yapp', [
    'ui.router',
    'ngRoute',
    'ngAnimate',
    'ngMaterial',
    'ngMdIcons',
    'oc.lazyLoad',
    'nvd3',
    'mdDataTable',
    'iso.directives',
    'ngFileUpload'
])
.provider('globalConfig', function () {

    var values = {
        urlLoadDevices: 'http://localhost:9090',
        loadDevicesDemo: true,
        urlTask:'http://localhost:9090',
        
        deviceDemo: [
             {IP_ADDRESS:'localhost',
              BRANCH_NAME:'SA',
              GROUP_DESCRIPTION:'Demo',
              user:'sa',
              password:''},
              {IP_ADDRESS:'localhost',
              BRANCH_NAME:'USER',
              GROUP_DESCRIPTION:'Demo',
              user:'user1',
              password:''},
              {IP_ADDRESS:'173.1.29.123',
              BRANCH_NAME:'DEMO 2',
              GROUP_DESCRIPTION:'Demo',
              user:'fresh.ftp',
              password:'HAwFMyyBYAN1jtni8sWc'}
            ]

    }
    return {
        $get: function () {
            return values;
        },
        set: function (constants) {
            angular.extend(values, constants);
        }
    };
})
.filter('humanReadableFileSize', ['$filter', function ($filter) {
    // See https://en.wikipedia.org/wiki/Binary_prefix
    var decimalByteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
    var binaryByteUnits = ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];

    return function (input) {
        var i = -1;
        var fileSizeInBytes = input;

        do {
            fileSizeInBytes = fileSizeInBytes / 1024;
            i++;
        } while (fileSizeInBytes > 1024);

        var result = decimalByteUnits[i];
        return Math.max(fileSizeInBytes, 0.1).toFixed(1) + ' ' + result;
    };
} ])
.config(function ($stateProvider, $urlRouterProvider, $mdThemingProvider, $mdIconProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/overview');


    $stateProvider
      .state('base', {
          abstract: true,
          url: '',
          templateUrl: 'views/base.html'
      })
    .state('base.web', {
        abstract: true,
        url: '',
        templateUrl: 'views/webBase.html'
    })
    .state('base.application', {
        abstract: true,
        url: '',
        parent: 'base',
        templateUrl: 'views/system.html',
        controller: 'BaseSysCtrl'
    })
    .state('home', {
        url: '',
        parent: 'base.web',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/controllers/home.js',
                        '/scripts/owl.carousel.min.js'
                    ]
                })
            }
        }
    })
    .state('login', {
        url: '/login',
        parent: 'base.web',
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
    })
    .state('base.application.overview', {
        url: '/overview',
        parent: 'base.application',
        templateUrl: 'views/dashboard/overview.html'
    })
    .state('base.application.FTP', {
        url: '/FTP',
        parent: 'base.application',
        templateUrl: 'views/FTP/main.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/controllers/ftpMainCtrl.js'
                    //'/scripts/controllers/ftpTabsCtrl.js',
                    //'/scripts/controllers/ftpCtrl.js'

                    ]
                })
            }
        }
    })
    .state('base.application.task', {
        url: '/task',
        parent: 'base.application',
        templateUrl: 'views/task/main.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/task/taskCtrl.js'
                    ]
                })
            }
        }
    })
    .state('base.application.virtualization', {
        url: '/virtualization',
        parent: 'base.application',
        templateUrl: 'views/virtualization/index.html',
        controller: 'testChart1Ctrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/virtualization/virtualizationCtrl.js'
                    ]
                })
            }
        }
    })
    .state('base.application.testChart1', {
        url: '/testChart1',
        parent: 'base.application',
        templateUrl: 'views/virtualization/testChart1.html',
        controller: 'testChart1Ctrl',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/virtualization/testChart1.js'
                    ]
                })
            }
        }
    })
    .state('base.application.monitoringGroup', {
        url: '/monitoringGroup',
        parent: 'base.application',
        templateUrl: 'views/monitoring/monitoringGroup.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/monitoring/monitoringGroupCtrl.js'
                    ]
                })
            }
        }
    })
    .state('base.application.debugFTP', {
        url: '/TestFTP',
        parent: 'base.application',
        templateUrl: 'views/FTP/testLayout.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/controllers/ftpMainCtrl.js',
                        '/scripts/controllers/ftpTabsCtrl.js',
                        '/scripts/controllers/ftpCtrl.js'
                    ]
                })
            }
        }
    })
    .state('base.application.SingleFTP', {
        url: '/SingleFTP',
        parent: 'base.application',
        templateUrl: 'views/FTP/testLayoutBase.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        '/scripts/controllers/ftpm.js'
                    ]
                })
            }
        }
    })
    .state('debugFTP', {
        url: '/debugFTP',
        parent: 'base.web',
        templateUrl: 'views/FTP/testLayoutBase.html'
    })

    .state('base.application.user', {
        url: '/user',
        parent: 'base.application',
        templateUrl: 'views/testMaterial.html',
        resolve: {
            loadMyFiles: function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'yapp',
                    files: [
                        'scripts/services/testMaterialService.js',
                        'scripts/controllers/test-material.js',

                    ]
                })
            }
        }
    })
    .state('base.application.reports', {
        url: '/reports',
        parent: 'base.application',
        templateUrl: 'views/dashboard/reports.html'
    });

    $mdIconProvider
                      .defaultIconSet("styles/svg/avatars.svg", 128)
                      .icon("menu", "styles/svg/menu.svg", 24)
                      .icon("share", "styles/svg/share.svg", 24)
                      .icon("google_plus", "styles/svg/google_plus.svg", 512)
                      .icon("hangouts", "styles/svg/hangouts.svg", 512)
                      .icon("twitter", "styles/svg/twitter.svg", 512)
                      .icon("phone", "styles/svg/phone.svg", 512);

    $mdThemingProvider.theme('default')
                      .primaryPalette('light-blue')
                      .accentPalette('orange');

});

