﻿
'use strict';

angular.module('FileManagerApp').config(['fileManagerConfigProvider', function (config) {
        var defaults = config.$get();
        config.set({
            appName: '',
            allowedActions: angular.extend(defaults.allowedActions, {
                remove: true
            })
        });
}]);
angular.module('yapp')
.config(function ($mdIconProvider) {
    $mdIconProvider
    .iconSet('communication', 'img/icons/sets/communication-icons.svg', 24);
})
.controller('ftpMainCtrl', function ($scope, $http, globalConfig) {

    $scope.list =
    {
        total: 100,
        pageCount: 10,
        pageSize: 50,
        pageIndex: 0,
        page: 1,
        data: [
        ],
        pushPage: []
    };
    $scope.FTPTabs = [];
    $scope.ConnectionFTP = function (device) {
        console.log("Connection FTP");
        console.log(device);
        $scope.FTPTabs.push({ device: device });
        //Create tab for file manager.
    }
    $scope.openPage = function (page) {
        //Event tab select
        console.log("load page");
        console.log(page);

    }
    function CreateTab(total, pageIndex, devices) {
        $scope.list.total = total;
        $scope.list.pageIndex = pageIndex;
        $scope.list.pageCount = (total / $scope.list.pageSize) + 1;
        for (var i = 1; i < $scope.list.pageCount; i++) {
            var pushDevices = devices.slice($scope.list.pageSize * (i - 1), $scope.list.pageSize * i);
            /*for (var j = 0; j < pushDevices.length; j++) {
            var d = new Date(pushDevices[j].SVC_TIME_STAMP);
            var curr_date = d.getDate();
            var curr_month = d.getMonth();
            var curr_year = d.getFullYear();
            pushDevices[j].LastDate = curr_date + "-" + curr_month + "-" + curr_year;
            }*/

            $scope.list.pushPage.push({ index: i, loaded: false, devices: pushDevices });
        }

    }
    function inital() {
        $http.get(globalConfig.urlLoadDevices + "/api/FTPClient/", {})
                .then(function (result) {
            CreateTab(result.data.length, 0, result.data);
        });
        /*$http.post('http://localhost:53568/api/monitoring/GetListByStatus?pageNumber=0&pageSize=0', {})
                .then(function (result) {

            CreateTab(result.data.TotalCount, 0, result.data.Results);
        });*/
        /*if (globalConfig.loadDevicesDemo) {
            CreateTab(globalConfig.deviceDemo.length, 0, globalConfig.deviceDemo);

        } else { 
        //Take these to service
        $http.post('http://localhost:53568/api/monitoring/GetListByStatus?pageNumber=0&pageSize=0', {})
                .then(function (result) {
                    CreateTab(result.data.TotalCount, 0, result.data.Results);
                });
        }*/
        
    }
    /*$http.post('http://localhost:53568/api/monitoring/GetListByStatus?pageNumber=' + offset + '&pageSize=' + pageSize, {})
    .then(function (result) {
    return {
    results: result.data.Results,
    totalResultCount: result.data.TotalCount
    }
    })*/

    inital();
});