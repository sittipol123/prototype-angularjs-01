﻿'use strict';


/**
   * Main Controller for the Angular Material Starter App
   * @param $scope
   * @param $mdSidenav
   * @param avatarsService
   * @constructor
   */
  function UserController(userService, $mdSidenav, $mdBottomSheet, $log,$q, $http) {
    var self = this;
    
    self.selected = null;
    self.users = [];
    self.selectUser = selectUser;
    self.toggleList = toggleUsersList;
    self.makeContact = makeContact;
    self.loadFtp = loadFtp;
    // Load all registered users
    self.http = $http;
    userService
          .loadAllUsers()
          .then(function (users) {
        self.users = [].concat(users);
        self.selected = users[0];
    });
    
    // *********************************
    // Internal methods
    // *********************************
    
    /**
     * Hide or Show the 'left' sideNav area
     */
    function toggleUsersList() {
        $mdSidenav('left').toggle();
    }
    
    /**
     * Select the current avatars
     * @param menuId
     */
    function selectUser(user) {
        self.selected = angular.isNumber(user) ? $scope.users[user] : user;
    }
    
    /**
     * Show the Contact view in the bottom sheet
     */
    function makeContact(selectedUser) {
        
        $mdBottomSheet.show({
            controllerAs  : "cp",
            templateUrl   : '/views/contactSheet.html',
            controller    : ['$mdBottomSheet', ContactSheetController],
            parent        : angular.element(document.getElementById('content'))
        }).then(function (clickedItem) {
            $log.debug(clickedItem.name + ' clicked!');
        });
        
        /**
         * User ContactSheet controller
         */
        function ContactSheetController($mdBottomSheet) {
            this.user = selectedUser;
            this.actions = [
                { name: 'Phone'       , icon: 'phone'       , icon_url: '/styles/svg/phone.svg' },
                { name: 'Twitter'     , icon: 'twitter'     , icon_url: '/styles/svg/twitter.svg' },
                { name: 'Google+'     , icon: 'google_plus' , icon_url: '/styles/svg/google_plus.svg' },
                { name: 'Hangout'     , icon: 'hangouts'    , icon_url: '/styles/svg/hangouts.svg' }
            ];
            this.contactUser = function (action) {
                // The actually contact process has not been implemented...
                // so just hide the bottomSheet
                
                $mdBottomSheet.hide(action);
            };
        }

  
    }
    /** Test post FTP  handle**/
    function loadFtp() {
        
        var data = JSON.stringify({ "action": "list", "path": "/" });
        this.http.get("http://localhost/ftpapi/handler.php")
             .success(function (data, status) {
           hello = data;
        });
        //$http.post("http://localhost/ftpapi/handler.php", data)
        //     .success(function (data, status) {
        //         $scope.hello = data;
        //});
        //.success(function (data) {
        //    dfHandler(data, deferred);
        //}).error(function (data) {
        //    dfHandler(data, deferred, 'Unknown error listing, check the response');
        //})['finally'](function () {
        //    self.inprocess = false;
        //});
    }
}
// Prepare the 'users' module for subsequent registration of controllers and delegates
angular
       .module('yapp')
       .controller('UserController', [
    'userService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', '$http',
    UserController
]);

