﻿'use strict';

angular.module('FileManagerApp').config(['fileManagerConfigProvider', function (config) {
        var defaults = config.$get();
        config.set({
            appName: '',
            allowedActions: angular.extend(defaults.allowedActions, {
                remove: true
            })
        });
}]);

