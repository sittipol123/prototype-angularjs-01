(function (window, angular) {
    'use strict';
    angular.module('yapp').controller('taskCtrl', function ($scope, $http, $mdDialog, $mdMedia, globalConfig) {
        $scope.dialogShowListDevice = false;
        $scope.taskList = [];
        $scope.openTask = function (ev,task) { 
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                controller: DialogDetailController,
                templateUrl: '/views/task/TaskDialogDetailTemplate.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: useFullScreen,
                locals : {
                    task : task
                }
            })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
            $scope.$watch(function () {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function (wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.addDownloadTask = function (ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                controller: DownloadTaskDialogController,
                templateUrl: '/views/task/DownloadTaskDialogTemplate.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: useFullScreen
            })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.addUploadTask = function (ev) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                controller: UploadTaskDialogController,
                templateUrl: '/views/task/UploadTaskDialogTemplate.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: useFullScreen
            })
            .then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
            $scope.$watch(function () {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function (wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };
        $scope.reRun = function (task) { 
            task.reRunLoading = true;
            $http.put(globalConfig.urlLoadDevices + "/api/task/rerun?id="+task.ID, {})
                .then(function (result) {
                    //No has verify 
                    task.reRunLoading = false;
            });
        }
        function reloadTask() { 
            $http.get(globalConfig.urlLoadDevices + "/api/task/", {})
                .then(function (result) {
                if ($scope.taskList.length != result.data.length) {
                    $scope.taskList = result.data;
                } else { 

                    for (var i = 0; i < $scope.taskList.length; i++) {
                        for (var j = 0; j < result.data.length; j++) {
                            if ($scope.taskList[i].ID == result.data[j].ID) {
                                $scope.taskList[i].name = result.data[j].name;
                                $scope.taskList[i].mainprogress = result.data[j].mainprogress;
                                $scope.taskList[i].status = result.data[j].status;
                                $scope.taskList[i].totalJob = result.data[j].totalJob;
                                $scope.taskList[i].compeleteJob = result.data[j].compeleteJob;
                                $scope.taskList[i].description = result.data[j].description;
                                $scope.taskList[i].failJob = result.data[j].failJob;
                                
                            }
                        }                                              
                    }                 
                }
                
            }); 
        } 
        function inital() {            
            setInterval(function () { 
                reloadTask();
            }, 1000);
        }
        inital();                  
    });
    function DialogDetailController($scope, $http, $mdDialog, Upload, globalConfig, task) {
        $scope.jobs = [];
        $scope.jobFiles = [];
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.ShowFiles = function (job) { 
            $scope.jobFiles = job.jobFiles;
        }
        function inital() {
            $http.get(globalConfig.urlLoadDevices + "/api/task?id="+task.ID)
             .then(function (result) {
                if (result.status == 200) { 
                    $scope.jobs = result.data.jobs;
                }                
            });
        }
        inital();
    }
    function DownloadTaskDialogController($scope, $http, $mdDialog, Upload, globalConfig) { 
        $scope.list = {
            total: 100,
            pageCount: 10,
            pageSize: 50,
            pageIndex: 0,
            page: 1,
            data: [
            ],
            pushPage: []
        };
        $scope.newJob = {
            TARGETPART: "",
            SOURCEPART: ""
        };
        $scope.uploadFileList = [];
        $scope.task = {
            NAME: "",
            DESCRIPTION: "",
            STATUS: "",
            JOBs: []
        }
        $scope.selectDevices = [];
        $scope.uploadFileList = [];
        $scope.hide = function () { 
            $mdDialog.hide();
        }
        $scope.start = function () {
            addDownloadTask('N');
            $mdDialog.hide();
        }
        $scope.addFile = function () {
            $scope.uploadFileList.push({
                TARGETPART: $scope.newJob.TARGETPART,
                SOURCEPART: $scope.newJob.SOURCEPART,
                isDOWNLOAD: true,
                isSERVER:true
                });
        }
        
        $scope.addToList = function (device) {
            $scope.selectDevices.push(device);
        }

        $scope.start = function () {
            addDownloadTask('N');
            $mdDialog.hide();
        }
        function addDownloadTask(status) {
            var task = {
                name: $scope.task.NAME,
                description: $scope.task.DESCRIPTION,
                status: status,
                jobs: []
            };
           
            for (var i = 0; i < $scope.selectDevices.length; i++) {
                
                var job = {
                    IP: $scope.selectDevices[i].IP,
                    STATUS: status,
                    FTP_USER: $scope.selectDevices[i].FTP_USER,
                    jobFiles: $scope.uploadFileList
                };
                task.jobs.push(job);               
            };
            
            $http.post(globalConfig.urlLoadDevices + "/api/task/", task)
            .then(function (result) { 
                
            });
        }

        function inital() {
            //Take these to service                        
            $http.get(globalConfig.urlLoadDevices + "/api/FTPClient/", {})
                .then(function (result) {
                CreateTab(result.data.length, 0, result.data);
            });

            
        }
        
        function CreateTab(total, pageIndex, devices) {
            $scope.list.total = total;
            $scope.list.pageIndex = pageIndex;
            $scope.list.pageCount = (total / $scope.list.pageSize) + 1;
            for (var i = 1; i < $scope.list.pageCount; i++) {
                var pushDevices = devices.slice($scope.list.pageSize * (i - 1), $scope.list.pageSize * i);
                $scope.list.pushPage.push({ index: i, loaded: false, devices: pushDevices });
            }
        }

        inital();
    }

    function UploadTaskDialogController($scope, $http, $mdDialog, Upload, globalConfig) {
        $scope.list = {
            total: 100,
            pageCount: 10,
            pageSize: 50,
            pageIndex: 0,
            page: 1,
            data: [
                ],
            pushPage: []
        };
        
        $scope.task = {
            NAME: "",
            DESCRIPTION: "",
            STATUS:"",
            JOBs: []
        }
        $scope.selectDevices = [];
        $scope.uploadFileList = [];
        $scope.hide = function () {
            $mdDialog.hide();
        };
        $scope.cancel = function () {
            $mdDialog.cancel();
        };
        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };
        $scope.addForUpload = function ($files) {
            $scope.uploadFileList = $scope.uploadFileList.concat($files);
        };
        $scope.addToList = function (device) {
            $scope.selectDevices.push(device);
        }
        $scope.start = function () {
            addUploadTask('N');
            $mdDialog.hide();
        }
        $scope.saveTask = function () {
            addUploadTask('P');
        }
        $scope.addDownloadTask = function () { 
        
        }
        function addUploadTask(status) {

            var taskData = {
                task: $scope.task,
                files: $scope.uploadFileList,
                devices: $scope.selectDevices
            };

            var count = $scope.selectDevices.length;
            $scope.task.STATUS = status;

            for (var i = 0; i < count; i++) {
                //new Jobs
                var job = {
                    IP: $scope.selectDevices[i].IP,
                    STATUS: status,
                    FTP_USER: $scope.selectDevices[i].FTP_USER
                };
                $scope.task.JOBs.push(job);
            }

            Upload.upload({
                url: globalConfig.urlTask + "/api/task?name="+$scope.task.NAME+"&target="+$scope.task.target,
                data: { AddTaskModel: angular.toJson($scope.task) },
                file: $scope.uploadFileList
            })
            .then(function (response) {
                console.log(" status: 200");
                
            }, function (err) {
                console.log("Error status: " + err.status);

            });
        }
        $scope.FTPTabs = [];
        function inital() {
            //Take these to service                        
            $http.get(globalConfig.urlLoadDevices + "/api/FTPClient/", {})
                .then(function (result) {
                    CreateTab(result.data.length, 0, result.data);
            });

            
        }
        function CreateTab(total, pageIndex, devices) {
            $scope.list.total = total;
            $scope.list.pageIndex = pageIndex;
            $scope.list.pageCount = (total / $scope.list.pageSize) + 1;
            for (var i = 1; i < $scope.list.pageCount; i++) {
                var pushDevices = devices.slice($scope.list.pageSize * (i - 1), $scope.list.pageSize * i);
                $scope.list.pushPage.push({ index: i, loaded: false, devices: pushDevices });
            }
        }
        inital();
    }

} (window, angular));