var  gulp = require( 'gulp' ),
 gulpDocs = require( 'gulp-ngdocs' ),
   config = require( './config' );
webserver = require( 'gulp-connect' );

gulp.task( 'docs', function() {

  return gulp
    .src( config.paths.src.js )
      .pipe( gulpDocs.process(
          {
            'html5Mode': false,
            'title': 'KO App Doc'
          }
    ) )
    .pipe( gulp
      .dest( config.paths.dist.maindir + '../_ngDocs/' )
    );

});

gulp.task( 'docs-server', function() {

  webserver.server({
    root: '_docs/',
    livereload: true
  });

});
