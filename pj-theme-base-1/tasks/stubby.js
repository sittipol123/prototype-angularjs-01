var gulp = require( 'gulp' ),
    stubby = require( 'gulp-stubby-server' ),
    gutil = require( 'gulp-util' ),
    config = require( './config' );


gulp.task( 'stubby', function( cb ) {

  if ( !config.isDev || config.isStaging || config.deployment ) { return cb(); }

  var options = {
    callback: function( server, options ) {

      server.get( 1, function( err, endpoint ) {

        if ( err ) {
          gutil.log( gutil.colors.red( 'Error Mocking: ' ) );
          gutil.log( gutil.colors.yellow( err ) );
        }

      });

    },
    stubs: 9000,
    tls: 9050,
    location: config.localIp,
    admin: 9100,
    relativeFilesPaths: true,
    mute: !!config.isTravis,
    persistent: true,
    files: [
      config.paths.mocks
    ]
  };

  stubby( options );

  cb();

});
