var gulp = require( 'gulp' ),
    jade = require( 'gulp-jade' ),
 htmlmin = require( 'gulp-minify-html' ),
 jsCache = require( 'gulp-ng-html2js' ),
  gulpif = require( 'gulp-if' ),
  uglify = require( 'gulp-uglify' ),
  concat = require( 'gulp-concat' ),
  sizeOf = require( 'gulp-size' ),
  config = require( './config' );

gulp.task( 'template', function() {

  return gulp
    .src( config.paths.src.template )
    .pipe( jade({
      'pretty': config.isDev,
      'locals': {
        'prod': !config.isDev,
        'version': config.paths.version,
        'subdir': config.paths.dist.subdir
}
    }) )
    .pipe( htmlmin({
      'empty': true,
      'spare': true,
      'quotes': true
    }) )
    .pipe( jsCache({
      'moduleName': 'templates',
      'standalone': true,
      'stripPrefix': 'components/'
    }) )
    .pipe( concat( 'templates.js' ) )
    .pipe( uglify( config.uglify ) )
    .pipe( sizeOf({
      'title': 'Templates -> ',
      'gzip': true
    }) )
    .pipe( gulp
      .dest( config.mainDist + config.paths.dist.template )
  );

});

gulp.task( 'template-reload', [ 'template' ], function() { config.bSync.reload(); } );
