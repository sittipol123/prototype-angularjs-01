var gulp = require( 'gulp' ),
    jade = require( 'gulp-jade' ),
      fs = require( 'fs' ),
    path = require( 'path' ),
  gulpif = require( 'gulp-if' ),
  incSrc = require( 'gulp-include-source' ),
 htmlmin = require( 'gulp-minify-html' ),
  ngHtml = require( 'gulp-angular-htmlify' ),
  sizeOf = require( 'gulp-size' ),
  config = require( './config' );



gulp.task( 'html', [ 'bower', 'template', 'js', 'css' ], function() {

  return gulp
    .src( config.paths.src.html[0] )
    .pipe( jade({
      'cache': false,
      'pretty': true,
      'locals': {
        'prod': !config.isDev,
        'staging': config.isStaging,
        'useBanner': ( !!config.isStaging || !!config.isDev ),
        'color': ( config.isStaging ) ? 'firebrick' : 'orange',
        'appName': config.paths.appName,
        'lang': config.paths.language,
        'subdir': config.paths.dist.subdir,
        'host': config.apiHost,
        'title': config.paths.title,
        'timeout': config.paths.timeout,
        'version': config.paths.version,
        'routeNamespace': config.paths.routeNamespace,
        'partitionID': config.paths.partitionID,
        'dateFormat': config.paths.dateFormat
      }
    }) )
    .pipe( gulpif( !config.isDev, ngHtml({
      'verbose': false
    }) ) )
    .pipe( incSrc({
      'cwd': config.paths.dist.maindir
      }) )
    .pipe( gulpif( !config.isDev, htmlmin({
      'collapseWhitespace': true,
      'removeComments': true,
      'minifyCSS': true
    }) ) )
    .pipe( sizeOf({
      'title': 'Html -> ',
      'gzip': true
    }) )
    .pipe( gulp
      .dest( config.paths.dist.maindir + config.paths.dist.html )
    );


});

gulp.task( 'html-reload', [ 'html' ], function() { config.bSync.reload(); } );
