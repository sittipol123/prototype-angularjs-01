var gulp    = require( 'gulp' ),
    gutil   = require( 'gulp-util' ),
    jConfig = require( './config.json' ),
   ipLookup = require( 'my-local-ip' ),
browserSync = require( 'browser-sync' ).create(),
 isTravis   = gutil.env.travis,
  isStaging = gutil.env.staging,
  isDeploy  = gutil.env.deploy,
  localIp   = !isTravis ? ipLookup() : '0.0.0.0',
  isDev     = !gutil.env.prod && !isStaging,
  distPath  = jConfig.dist.maindir + jConfig.dist.subdir;
  useMangle = isDev ? false : { 'except': [ 'angular', 'd3' ] };

module.exports.paths = jConfig;
module.exports.mainDist = distPath;

module.exports.deployment = isDeploy;
module.exports.isDev = isDev;
module.exports.isTravis = isTravis;
module.exports.isStaging = isStaging;

// Global created browserSync reference
module.exports.bSync = browserSync;

// Sets up a local ip for watch only
module.exports.localIp = localIp;


/**
 *  Default javascript compress arguments
 */

module.exports.uglify = {

  'mangle': useMangle,
  'enclose': true,
  'compress': {
    'drop_debugger': !isDev,
    'drop_console': !isDev
  },
  'preserveComments': !isDev,
  'unsafe': true,
  'output': {
    'beautify': isDev
  }

};


// Checks to see what API we need to be on
module.exports.apiHost = process.env.API_SERVER || 'https://' + localIp + ':9050';

if ( isStaging && !process.env.API_SERVER && jConfig.stagingHost ) {
  module.exports.apiHost = jConfig.stagingHost;
}

