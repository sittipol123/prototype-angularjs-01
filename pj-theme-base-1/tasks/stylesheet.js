var gulp = require( 'gulp' ),
  scss = require( 'gulp-sass' ),
  gulpif = require( 'gulp-if' ),
  cssMin = require( 'gulp-minify-css' ), /** @see http://goalsmashers.github.io/css-minification-benchmark/ */
  concat = require( 'gulp-concat' ),
  cssGlob = require( 'gulp-css-globbing' ),
  sMaps = require( 'gulp-sourcemaps' ),
  sizeOf = require( 'gulp-size' ),
  config = require( './config' );

gulp.task( 'css', function() {


  var bowerIncludes = [
    config.paths.bower + '/bootstrap-sass/assets/stylesheets',
    config.paths.bower + '/bourbon/app/assets/stylesheets'
  ];

  return gulp
    .src( config.paths.src.css )
    .pipe( cssGlob({
      extensions: [ '.scss' ]
    }) )
      .pipe( gulpif( config.isDev, sMaps.init() ) )
          .pipe( scss({
            'outputStyle': ( config.isDev ? 'expanded' : 'compressed' ),
            'includePaths': bowerIncludes
          }) )
      .pipe( gulpif( config.isDev, sMaps.write( 'maps' ) ) )
    .pipe( gulpif( !config.isDev, concat( 'style.css' ) ) )
    .pipe( gulpif( !config.isDev, cssMin( {
      'advanced': true,
      'keepSpecialComments': 0,
      'sourceMap': false
    } ) ) )
    .pipe( sizeOf({
      'title': 'Stylesheets -> ',
      'gzip': true
    }) )
    .pipe( gulp
      .dest( config.mainDist + config.paths.dist.css )
  );

});


gulp.task( 'css-reload', [ 'css' ], function() { config.bSync.reload(); } );
