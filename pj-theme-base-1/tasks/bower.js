var gulp = require( 'gulp' ),
  bFiles = require( 'main-bower-files' ),
  concat = require( 'gulp-concat' ),
  gulpif = require( 'gulp-if' ),
  uglify = require( 'gulp-uglify' ),
  sizeOf = require( 'gulp-size' ),
  config = require( './config' );

gulp.task( 'bower', function() {

  return gulp
    .src( bFiles({
      'env': config.isDev ? 'development' : 'production',
      'filter': '**/**/*.js'
    }) )
    .pipe( concat( 'public.js' ) )
    .pipe( uglify( config.uglify ) )
    .pipe( sizeOf({
      'title': 'Packages -> ',
      'gzip': true
    }) )
    .pipe( gulp.dest( config.mainDist + config.paths.dist.public ) );

});
