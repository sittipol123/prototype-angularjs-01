var gulp = require( 'gulp' ),
   gutil = require( 'gulp-util' ),
  config = require( './config' );

gulp.task( 'default', [ 'css', 'bower', 'js', 'template', 'docs', 'html', 'assets' ], function() {


  if ( !config.isDev && !config.isStaging ) {
    gutil.log( gutil.colors.red( 'Running in Production' ) );
  } else if ( config.isStaging ) {
    gutil.log( gutil.colors.blue( 'Running in Staging' ) );
  } else {
    gutil.log( gutil.colors.green( 'Running in Development' ) );
  }

  // Handles the deployment message to the user so that they know at the end
  if ( config.deployment ) {
    gutil.log( gutil.colors.bgRed.bold.black( '\n\nYour Fresh Deployment Is Now Complete!\n' ) );
  }


});
