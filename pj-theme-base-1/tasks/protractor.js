var gulp = require( 'gulp' ),
   karma = require( 'gulp-karma' ),
   gutil = require( 'gulp-util' ),
  protractor = require( 'gulp-protractor' ).protractor,
  histApi = require( 'connect-history-api-fallback' ),
config = require( './config' );

gulp.task( 'protractor', [ 'stubby', 'default' ], function( cb ) {

  /* Starts the server uniquely for protractor */
  config.bSync.init({

    'open': false,
    'server': config.paths.dist.maindir,
    'middleware': [ histApi() ],
    'logLevel': 'silent',
    'codeSync': false,
    'injectChanges': false,
    'port': '3000'

  });

  gulp.src( config.paths.test.tests )
    .pipe( protractor({
            'seleniumServerJar':
                './node_modules/protractor/selenium/selenium-server-standalone-2.45.0.jar',
            'configFile': 'tests/protractor.conf.js',
            'args': [ '--baseUrl', 'http://localhost:3000' + config.paths.routeNamespace ]
          })
    )
    .on( 'error', function( e ) {
      throw e;
    })
    .on( 'end', function() {
        config.bSync.exit();
        cb();
    } );

});
