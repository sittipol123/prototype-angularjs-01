var gulp = require( 'gulp' ),
   gutil = require( 'gulp-util' ),
  config = require( './config' ),
  cLog = require( 'connect-logger' ),
  histApi = require( 'connect-history-api-fallback' );

gulp.task( 'watch', [ 'default', 'stubby' ], function() {

  if ( ( !config.isDev && !config.isStaging ) || config.deployment ) { return; }



    config.bSync.init({

        'open': false,
        'server': config.paths.dist.maindir,
        'middleware': [
          histApi(),
          cLog({
            'format': '[ %method / %status ] %url ( %time )'
          })
        ],
        'logConnections': true,
        'logLevel': 'warn',
        'host': config.localIp,
        'proxy': config.paths.vhost,
        'https': true

    });

    gulp.watch( config.paths.src.css, [ 'css-reload' ] );

    gulp.watch( config.paths.src.js, [ 'js-reload', 'docs' ] );

    gulp.watch( config.paths.src.html, [ 'html-reload' ]);

    gulp.watch( config.paths.src.template, [ 'template-reload' ] );

    gulp.watch( config.paths.src.assets, [ 'assets-reload' ] );

});
