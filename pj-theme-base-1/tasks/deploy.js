var gulp     = require( 'gulp' ),
  gzip       = require( 'gulp-zip' ),
  gutil      = require( 'gulp-util' ),
  mv         = require( 'node-mv' ),
  del        = require( 'del' ),
  config     = require( './config' );

gulp.task( 'deploy', [ 'css', 'bower', 'js', 'template', 'html', 'assets' ], function( cb ) {

  gutil.log( gutil.colors.bgRed.bold.black(
    '\n\nKO Deployment Package is Ready!\n'
  ) );

  // Moves the UI directory into the current directory since they serve it out of UI/
  gulp.src( config.paths.dist.maindir + config.paths.dist.subdir + '/**/*' )
    .pipe( gulp.dest( config.paths.dist.maindir ) );

  gulp.src(
    [ 'src/**/*', 'tasks/**/*', 'tests/**/*', '.jscsrc', '.bowerrc', 'README.md',
      'gulpfile.js', 'package.json', 'bower.json', config.paths.dist.maindir + '/**/*' ],
    { base: '.' } )
    .pipe(
    gzip( '../KO-' + config.paths.version + '.zip' )
  )
    .pipe(
    gulp.dest( config.paths.dist.maindir )
  );


});
