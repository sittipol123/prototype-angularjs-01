var gulp = require( 'gulp' ),
   karma = require( 'gulp-karma' ),
   gutil = require( 'gulp-util' ),
  config = require( './config' );

gulp.task( 'tests', [ ], function() {

  gulp
    .src( './foo.bar' )
    .pipe( karma({
      'configFile': config.paths.test.config,
      'action': ( config.isDev && !config.isTravis ) ? 'watch' : 'run'
    }) )
    .on( 'error', function( err ) {
      if ( config.isTravis || !config.isDev ) {
        throw err;
      }
      gutil.log( 'Issues: ' + err );
    });

});
