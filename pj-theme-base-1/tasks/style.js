var gulp = require( 'gulp' ),
    jscs = require( 'gulp-jscs' ),
    config = require( './config' );

gulp.task( 'style', function() {

  return gulp
    .src( config.paths.src.js )
    .pipe( jscs() );

});
