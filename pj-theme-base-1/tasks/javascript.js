var gulp = require( 'gulp' ),
  jshint = require( 'gulp-jshint' ),
 stylish = require( 'jshint-stylish' ),
  gulpif = require( 'gulp-if' ),
  ngAnno = require( 'gulp-ng-annotate' ),
  uglify = require( 'gulp-uglify' ),
  concat = require( 'gulp-concat' ),
  sizeOf = require( 'gulp-size' ),
  config = require( './config' );

gulp.task( 'js', [ 'style' ], function() {

  return gulp
    .src( config.paths.src.js )
    .pipe( jshint() )
    .pipe( jshint.reporter( 'jshint-stylish' ) )
    .pipe( gulpif( config.isTravis, jshint.reporter( 'fail' ) ) )
    .pipe( ngAnno() )
    .pipe( gulpif( !config.isDev, concat( 'prod.js' ) ) )
    .pipe( gulpif( !config.isDev, uglify( config.uglify ) ) )
    .pipe( sizeOf({
      'title': 'App -> ',
      'gzip': true
    }) )
    .pipe( gulp
      .dest( config.mainDist + config.paths.dist.js )
    );

});

gulp.task( 'js-reload', [ 'js' ], function() { config.bSync.reload(); } );
