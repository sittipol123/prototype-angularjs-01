var gulp = require( 'gulp' ),
    config = require( './config' );

gulp.task( 'assets', function() {

  return gulp
    .src( config.paths.src.assets )
    .pipe( gulp
      .dest( config.mainDist + config.paths.dist.assets )
  );

});

gulp.task( 'assets-reload', [ 'assets' ], function() { config.bSync.reload(); } );
